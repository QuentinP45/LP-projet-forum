#Projet de forum avec le framework Symfony

## Installation du projet :

- Après avoir cloner le projet se rendre dans la racine du projet
- installer les dépendances avec :
- composer install
- lancer le symfony avec :
- symfony serve

## Installation de la BDD

La BDD du projet est en SQLITE, pour l'installer :
- bin/console doctrine:database:create
- bin/console make:migration

##Source MOCODO :

USER: id_user, pseudo, mail, date_inscription, is_modo, is_admin
CREER, ON USER, 11 SUJET
SUJET: id_sujet ,label, date_creation, description
CATEGORIE: id_categorie, label, description

ECRIRE, 0N USER, 11 COMMENTAIRE
LISTER, 0N SUJET, 11 COMMENTAIRE
APPARTENIR, 1N SUJET, 0N CATEGORIE

COMMENTAIRE: id_commentaire, date_creation, contenu
REPONDRE, 01 COMMENTAIRE, 0N COMMENTAIRE