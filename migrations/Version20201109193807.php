<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201109193807 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_67F068BC7C4D497E');
        $this->addSql('DROP INDEX IDX_67F068BCBA9CD190');
        $this->addSql('CREATE TEMPORARY TABLE __temp__commentaire AS SELECT id, commentaire_id, sujet_id, created_at, contenu FROM commentaire');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('CREATE TABLE commentaire (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, commentaire_id INTEGER DEFAULT NULL, sujet_id INTEGER NOT NULL, created_at DATETIME NOT NULL, contenu CLOB NOT NULL COLLATE BINARY, CONSTRAINT FK_67F068BCBA9CD190 FOREIGN KEY (commentaire_id) REFERENCES commentaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_67F068BC7C4D497E FOREIGN KEY (sujet_id) REFERENCES sujet (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO commentaire (id, commentaire_id, sujet_id, created_at, contenu) SELECT id, commentaire_id, sujet_id, created_at, contenu FROM __temp__commentaire');
        $this->addSql('DROP TABLE __temp__commentaire');
        $this->addSql('CREATE INDEX IDX_67F068BC7C4D497E ON commentaire (sujet_id)');
        $this->addSql('CREATE INDEX IDX_67F068BCBA9CD190 ON commentaire (commentaire_id)');
        $this->addSql('DROP INDEX IDX_83F3F754BCF5E72D');
        $this->addSql('DROP INDEX IDX_83F3F7547C4D497E');
        $this->addSql('CREATE TEMPORARY TABLE __temp__sujet_categorie AS SELECT sujet_id, categorie_id FROM sujet_categorie');
        $this->addSql('DROP TABLE sujet_categorie');
        $this->addSql('CREATE TABLE sujet_categorie (sujet_id INTEGER NOT NULL, categorie_id INTEGER NOT NULL, PRIMARY KEY(sujet_id, categorie_id), CONSTRAINT FK_83F3F7547C4D497E FOREIGN KEY (sujet_id) REFERENCES sujet (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_83F3F754BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO sujet_categorie (sujet_id, categorie_id) SELECT sujet_id, categorie_id FROM __temp__sujet_categorie');
        $this->addSql('DROP TABLE __temp__sujet_categorie');
        $this->addSql('CREATE INDEX IDX_83F3F754BCF5E72D ON sujet_categorie (categorie_id)');
        $this->addSql('CREATE INDEX IDX_83F3F7547C4D497E ON sujet_categorie (sujet_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_67F068BCBA9CD190');
        $this->addSql('DROP INDEX IDX_67F068BC7C4D497E');
        $this->addSql('CREATE TEMPORARY TABLE __temp__commentaire AS SELECT id, commentaire_id, sujet_id, created_at, contenu FROM commentaire');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('CREATE TABLE commentaire (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, commentaire_id INTEGER DEFAULT NULL, sujet_id INTEGER NOT NULL, created_at DATETIME NOT NULL, contenu CLOB NOT NULL)');
        $this->addSql('INSERT INTO commentaire (id, commentaire_id, sujet_id, created_at, contenu) SELECT id, commentaire_id, sujet_id, created_at, contenu FROM __temp__commentaire');
        $this->addSql('DROP TABLE __temp__commentaire');
        $this->addSql('CREATE INDEX IDX_67F068BCBA9CD190 ON commentaire (commentaire_id)');
        $this->addSql('CREATE INDEX IDX_67F068BC7C4D497E ON commentaire (sujet_id)');
        $this->addSql('DROP INDEX IDX_83F3F7547C4D497E');
        $this->addSql('DROP INDEX IDX_83F3F754BCF5E72D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__sujet_categorie AS SELECT sujet_id, categorie_id FROM sujet_categorie');
        $this->addSql('DROP TABLE sujet_categorie');
        $this->addSql('CREATE TABLE sujet_categorie (sujet_id INTEGER NOT NULL, categorie_id INTEGER NOT NULL, PRIMARY KEY(sujet_id, categorie_id))');
        $this->addSql('INSERT INTO sujet_categorie (sujet_id, categorie_id) SELECT sujet_id, categorie_id FROM __temp__sujet_categorie');
        $this->addSql('DROP TABLE __temp__sujet_categorie');
        $this->addSql('CREATE INDEX IDX_83F3F7547C4D497E ON sujet_categorie (sujet_id)');
        $this->addSql('CREATE INDEX IDX_83F3F754BCF5E72D ON sujet_categorie (categorie_id)');
    }
}
