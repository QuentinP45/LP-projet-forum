<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201108071622 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, label VARCHAR(255) NOT NULL, description CLOB NOT NULL)');
        $this->addSql('CREATE TABLE commentaire (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, commentaire_id INTEGER DEFAULT NULL, sujet_id INTEGER NOT NULL, created_at DATETIME NOT NULL, contenu CLOB NOT NULL)');
        $this->addSql('CREATE INDEX IDX_67F068BCBA9CD190 ON commentaire (commentaire_id)');
        $this->addSql('CREATE INDEX IDX_67F068BC7C4D497E ON commentaire (sujet_id)');
        $this->addSql('CREATE TABLE sujet (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, label VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, description CLOB NOT NULL)');
        $this->addSql('CREATE TABLE sujet_categorie (sujet_id INTEGER NOT NULL, categorie_id INTEGER NOT NULL, PRIMARY KEY(sujet_id, categorie_id))');
        $this->addSql('CREATE INDEX IDX_83F3F7547C4D497E ON sujet_categorie (sujet_id)');
        $this->addSql('CREATE INDEX IDX_83F3F754BCF5E72D ON sujet_categorie (categorie_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('DROP TABLE sujet');
        $this->addSql('DROP TABLE sujet_categorie');
    }
}
